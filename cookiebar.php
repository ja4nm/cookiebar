<?php
/**
 * Cookiebar plugin for EU cookie law
 *
 * by Jan M
 */

class Cookiebar{

    const COOKIE_NAME = "cookiebar_enabled";
    const EXPIRE_DAYS = 365;

    const COOKIE_NOT_SET = -1;
    const COOKIE_ENABLED = 1;
    const COOKIE_DISABLED = 0;

    /**
     * Returns if cookies are enabled.
     * @return int (-1 / 1 / 0)
     */
    public static function areCookiesEnabled(){
        $enabled = isset($_COOKIE[Cookiebar::COOKIE_NAME])? $_COOKIE[Cookiebar::COOKIE_NAME] : -1;
        $enabled = intval($enabled);
        return $enabled;
    }

    /**
     * Disable or enable cookies
     * @param int $enabled: -1 / 1 / 0
     */
    public static function setCookiesEnabled($enabled){
        $enabled = intval($enabled);
        if($enabled != Cookiebar::COOKIE_NOT_SET && $enabled != Cookiebar::COOKIE_ENABLED && $enabled != Cookiebar::COOKIE_DISABLED) return;

        if($enabled == -1){
            unset($_COOKIE[Cookiebar::COOKIE_NAME]);
            setcookie(Cookiebar::COOKIE_NAME, 0, -1, "/");
            return;
        }

        $_COOKIE[Cookiebar::COOKIE_NAME] = $enabled;
        $expire = time()+Cookiebar::EXPIRE_DAYS*24*60*60;
        setcookie(Cookiebar::COOKIE_NAME, $enabled, $expire, "/");
    }
}

/**
 * actions
 */
if(isset($_REQUEST['setCookiesEnabled'])){
    $enable = intval($_REQUEST['setCookiesEnabled']);
    Cookiebar::setCookiesEnabled($enable);
}
if(isset($_REQUEST['areCookiesEnabled'])){
    $enabled = Cookiebar::areCookiesEnabled();
    echo $enabled;
}

?>