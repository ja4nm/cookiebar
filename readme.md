# Cookiebar #

Minimalistic cookie banner plugin for EU cookie law written in javascript. It is easy to use, customize and change style. For help see examples below or look in *example.html*. More about EU cookie law: https://www.cookielaw.org/the-cookie-law/.

![Cookiebar look.](http://janm.si/other/cookiebar/cookiebar.png "Cookiebar look.")


## Simple example ##

This is the most basic example. It just displays cookiebar with default looks and default message. Cookiebar requires jQuery, cookiebar.css and cookiebar.js files to be included. Fore more options see Full example.

\*cookiebar.php is not required

    <!DOCTYPE html>
    <html>
    <head>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="cookiebar.css" />
        <script type="text/javascript" src="cookiebar.js"></script>
        
        <script type="text/javascript">
        $(document).ready(function(){
            Cookiebar.load($("#cookiebar_holder"));
        });
        </script>
    </head>
    <body>
        <div id="cookiebar_holder"></div>
    </body>
    </html>



## Full example ##

This is full example with all available options. You can change cookiebar consent, button values, animations and other options by using mehod `Cookiebar.inti()`. Cookiebar look can be changed by overwriting or modifying file `cookiebar.css`.  cookiebar.php is required only if `usePhp` is set to true.

    <!DOCTYPE html>
    <html>
    <head>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="cookiebar.css" />
        <script type="text/javascript" src="cookiebar.js"></script>
        
        <script type="text/javascript">
        Cookiebar.init({
            //cookiebar message
            text: 'Please enable <a href="more.html" target="_blank">cookes</a>.',
            //confirm button text
            confirmText: "ok",
            //close button text
            closeText: "close",
            //show confirm button
            confirmButton: true,
            //show close button
            closeButton: true,
            //refresh page after close/confirm button click
            refresh: false,
            //keep showing cookiebar until user enables cookies
            preserve: false,
            //cookiebar cookie expiration date (if "usePhp" is true, also change constant EXPIRE_DAYS in cookiebar.php)
            expireDays: 365,
            //cookie name for cookiebar (you can leave this at the default value)
            cookieName: "cookiebar_enabled",
            //set and get cookies using php (you probability wont need this)
            usePhp: false,
            //link to php script (useful only if "usePhp" is true)
            phpUrl: "http://localhost/cookiebar.php",
            //on load callback
            onLoad: function(){
                Cookiebar.slideDown();
                console.log("Cookiebar onLoad!");
            },
            //on confirm clallback
            onConfirm: function(){
                Cookiebar.slideUp();
                console.log("Cookiebar onConfirm!");
            },
            //on close callback
            onClose: function(){
                Cookiebar.slideUp();
                console.log("Cookiebar onClose!");
            }
        });
        Cookiebar.load($("#cookiebar_holder"));
        </script>
    </head>
    <body>
        <div id="cookiebar_holder"></div>
    </body>
    </html>


## Other handy methods ##

You can use methods below to check or set if cookies are enabled.

    //set cookies enabled
    Cookiebar.setCookiesEnabled(true);

    //check if cookies are enabled
    if(Cookiebar.areCookiesEnabled()){
        alert("Cookies are enabled.");
    }else {
        alert("Cookies are not enabled.");
    }

    //if "usePhp" is true you need to use callback to check if cookies are enabled
    Cookiebar.areCookiesEnabled(function (enabled) {
        alert(enabled);
    });


## Cookiebar animations. ##

Cookiebar has 2 default animations which you can use by calling methods below. However, there is nothing stooping you by adding your own animations.

    //show cookiebar
    Cookiebar.slideDown();

    //close cookiebar
    Cookiebar.slideUp();


---
By Jan M - http://janm.si
