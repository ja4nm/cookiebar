/**
 * Javascript Cookiebar plugin for EU cookie law by Jan M
 */

function Cookiebar(){}
Cookiebar._cookiebarHolder = null;

Cookiebar.options = {
    text: "By continuing to use the site, you agree to the use of cookies.",
    confirmText: "ok",
    closeText: "close",
    confirmButton: true,
    closeButton: true,
    refresh: false,
    preserve: false,
    cookieName: "cookiebar_enabled",
    expireDays: 365,
    usePhp: false,
    phpUrl: "http://localhost/cookiebar.php",
    onLoad: null,
    onConfirm: null,
    onClose: null
};

/**
 * helper, private functions
 */
Cookiebar._setCookie = function(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires+"; path=/";
};
Cookiebar._getCookie = function(cname){
    var name = cname + "=";
    var ca = document.cookie.split(';');

    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }

    return null;
};
Cookiebar._setCookiePhp = function(enabled){
    enabled = parseInt(enabled);

    $.ajax({
        type: "POST",
        url: Cookiebar.options.phpUrl,
        data: "setCookiesEnabled="+enabled,
    });
};
Cookiebar._getCookiePhp = function(callback){
    var enabled = -1;

    $.ajax({
        type: "POST",
        url: Cookiebar.options.phpUrl,
        data: "areCookiesEnabled=1",

        success: function(r){
            enabled = parseInt(r);
            callback(enabled);
        }
    });
};

/**
 * enable or disable cookies
 */
Cookiebar.setCookiesEnabled = function(enabled){
    if(Cookiebar.options.usePhp){
        Cookiebar._setCookiePhp(enabled);
    }

    else{
        Cookiebar._setCookie(Cookiebar.options.cookieName, enabled, Cookiebar.options.expireDays);
    }
};
Cookiebar.areCookiesEnabled = function(callback){
    if(Cookiebar.options.usePhp){
        Cookiebar._getCookiePhp(function(value){
            callback(value);
        });
    }

    else{
        var value = Cookiebar._getCookie(Cookiebar.options.cookieName);
        if(value == null) value = -1;
        value = parseInt(value);
        if(callback) callback(value);
        return value;
    }
};

/**
 * html layout
 */
Cookiebar.getCookiebarHtml = function(){
    var html = "";
    html += '<div class="cookiebar" style="display: none;">';
    html += '<div class="cookiebar_wrap">';
    html += '<div class="cookiebar_text">'+Cookiebar.options.text+'</div>';

    if(Cookiebar.options.closeButton)
        html += '<div class="cookiebar_close">'+Cookiebar.options.closeText+'</div>';
    if(Cookiebar.options.confirmButton)
        html += '<div class="cookiebar_confirm">'+Cookiebar.options.confirmText+'</div>';

        html += '<div class="cookiebar_clear"></div>';
    html += '</div>';
    html += '</div>';

    return html;
};

/**
 * animations
 */
Cookiebar.slideDown = function(){
    Cookiebar._cookiebarHolder.find(".cookiebar").slideDown(300);
};
Cookiebar.slideUp = function(){
    Cookiebar._cookiebarHolder.find(".cookiebar").slideUp(300);
};

/**
 * event listeners
 */
Cookiebar.options.onLoad = function(){
    Cookiebar.slideDown();
};
Cookiebar.options.onConfirm = function(){
    Cookiebar.slideUp();
};
Cookiebar.options.onClose = function(){
    Cookiebar.slideUp();
};

/**
 * bind events
 */
Cookiebar.enableCookies = function(){
    Cookiebar.setCookiesEnabled(1);
    Cookiebar.options.onConfirm();
    if(Cookiebar.options.refresh)
        window.location.href = window.location.href;
};
Cookiebar.disableCookies = function(){
    Cookiebar.setCookiesEnabled(0);
    Cookiebar.options.onClose();
};

/**
 * init
 */
Cookiebar.init = function(options){
    for(var key in options){
        Cookiebar.options[key] = options[key];
    }
};

/**
 * load
 */
Cookiebar.load = function(element){
    Cookiebar._cookiebarHolder = element;
    Cookiebar._cookiebarHolder.append(Cookiebar.getCookiebarHtml());

    //bind events
    Cookiebar._cookiebarHolder.on("click", ".cookiebar_confirm", function(){
        Cookiebar.enableCookies();
    });
    Cookiebar._cookiebarHolder.on('click', ".cookiebar_close", function(){
        Cookiebar.disableCookies();
    });

    if(Cookiebar.options.preserve){
        Cookiebar.areCookiesEnabled(function(value){
            if(value != 1) Cookiebar.options.onLoad();
        });
    }else{
        Cookiebar.areCookiesEnabled(function(value){
            if(value == -1) Cookiebar.options.onLoad();
        });
    }
};
